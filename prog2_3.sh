#!/bin/bash

echo "Assignment #2-3, Matthew Hess, Matthew9510@gmail.com"
argumentDirectory=$1
argumentDirectory2=$2
argumentDirectory3=$3
argumentDirectory4=$4

for file in $1 $2 $3 $4
do

if grep "%.3" $file > /dev/null # avoids prints
then
gcc -w "$file" -o "${file%.c}.out" -lm #fix warnings with w
echo "$file Assignemnt #1"

elif  grep "%.4" $file > /dev/null
then
gcc -w "$file" -o "${file%.c}.out" -lm
echo "$file Assignemnt #2"

elif grep "%.5" $file > /dev/null 
then
gcc -w "$file" -o "${file%.c}.out" -lm
echo "$file Assignemnt #3"

else
gcc -w "$file" -o "${file%.c}.out" -lm
echo "$file Assignemnt #4"

fi
done
