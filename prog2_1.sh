#!/bin/bash

echo "Assignment #2-1, Matthew Hess, Matthew9510@gmail.com"
tmpSearch=100,100,100
argumentGrades=$1
argumentLogins=$2
tmpFirstName=`grep $tmpSearch $argumentGrades | cut -d "," -f1` #Gives us (firstName,lastName,100,100,100)
tmpLastName=`grep $tmpSearch $argumentGrades | cut -d "," -f2` #Gives us (firstName,lastName,100,100,100)
tmpNameString="$tmpFirstName $tmpLastName"
echo `grep "$tmpNameString" $argumentLogins | cut -d "," -f1` #"$tmpNameString"!
echo `grep "$tmpNameString" $argumentLogins | cut -d "," -f2`
echo `grep "$tmpNameString" $argumentLogins | cut -d "," -f3`
