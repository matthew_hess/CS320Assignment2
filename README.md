# CSAssignment2
## Created by: Matthew Hess
---
### Project Description:
+ This second project is a set of very fishy programs. It modifies grades and steals assignments without a hassle.
+ This project tested my ability to learn yet another new programming language. I learned that google is a very useful tool. I also learned that writing scripts gives us the ability to use multiple more focused programs to create a custom program to do multiple tasks.

---
### prog2_1.sh
   This program is written in bash script. In order to run it, you will need to open up your nearest terminal and jump in the directory where prog2_1.sh, Grades, and the Login files are located.
   Then Run the program by typing the following line of code: `./prog2_1.sh Grades Logins`

   This Program:
  * Displays the First and Lastname, Git account, and their password all on three separate lines.

---
### prog2_2.sh
   This program is written in bash script. In order to run it, you will need to open up your nearest terminal and jump in the directory where prog2_2.sh file and the git CS320Assignment3 folder are located.
   
   Then
  * Run the program by typing the following line of code: `./prog2_2.sh /pathOfGitRepository`

   This Program:
  * Finds the c programs in the git folder, and copies the files recursively while adding the file type (.c) to the original file.

---
### prog2_3.sh
   This program is written in bash script. In order to run it, you will need to open up your nearest terminal and jump in the directory where prog2_2.sh file is located as well as four additional files that prog2_2.sh outputted after running it.
   
   Then
  * Run the program by typing the following line of code: `./prog2_3.sh prog2_2OutputFile1Name  prog2_2OutputFile2NAme prog2_2OutputFile3Name prog2_2OutputFile4Name`

   This Program:
  * Compiles the additional 4 files, and displays the assignment name for each individual file that corresponds. 
