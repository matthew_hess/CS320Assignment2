#!/bin/bash

echo "Assignment #2-2, Matthew Hess, Matthew9510@gmail.com"
argumentDirectory=$1
for p in `grep -lr "include" $argumentDirectory`
do
  cp $p $(basename $p).c
  echo $(basename $p).c
done
